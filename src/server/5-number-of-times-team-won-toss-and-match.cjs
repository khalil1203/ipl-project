const csvToJson = require('csvtojson');
const matchesPath = 'src/data/matches.csv';
const fs = require('fs');

const demo = () => {

    // fetching data from matches.csv file   
    csvToJson()
        .fromFile(matchesPath)
        .then((data) => {
            let ansData = [];

            data.map((same) => {
                found = false;

                //if match winner and toss winner is same!
                if (same.winner == same.toss_winner) {
                    for (let index = 0; index < ansData.length; index++) {
                        if (ansData[index].team == same.winner) {
                            ansData[index].count += 1;
                            found = true;
                            break;
                        }
                    }

                    //if data is not present in the array!
                    if (!found) {
                        let demoObj = {
                            team: same.winner,
                            count: 1
                        }
                        ansData.push(demoObj);
                    }
                }

            });

            // sorting the answer array!
            ansData.sort((prev, cur) => {
                if (prev.count < cur.count) {
                    return 1;
                }
                else if (prev.count > cur.count) {
                    return -1;
                }
                else {
                    return 0;
                }
            })

            console.log(ansData);
            fs.writeFileSync('src/public/output/5-number-of-times-team-won-toss-and-match.json', JSON.stringify(ansData));
        });
}
demo();

