const csvToJson = require('csvtojson');
const deliveriesPath = 'src/data/deliveries.csv';
const matchesPath = 'src/data/matches.csv';
const fs = require('fs');

const match_Id_Arr = [];

const demo = () => {
    // fetching data from matches.csv file
    csvToJson()
        .fromFile(matchesPath)
        .then((data) => {
            for (let index = 0; index < data.length; index++) {
                if (data[index].season == '2015')
                    match_Id_Arr.push(data[index].id);
            }
            console.log(match_Id_Arr);
        });

    // fetching data from deliveries.csv file
    csvToJson()
        .fromFile(deliveriesPath)
        .then((deliveriesData) => {
            let topTenEconomicalBaller = [];

            for (let index = 0; index < deliveriesData.length; index++) {

                // if data is present in answer array!
                if (match_Id_Arr.includes(deliveriesData[index].match_id)) {
                    let curBowler = deliveriesData[index].bowler;
                    let curRun = parseInt(deliveriesData[index].total_runs);
                    let found = false;


                    for (let index2 = 0; index2 < topTenEconomicalBaller.length; index2++) {
                        if (topTenEconomicalBaller[index2].bowler == curBowler) {
                            topTenEconomicalBaller[index2].runs += curRun;
                            topTenEconomicalBaller[index2].ball += 1;

                            let curEconomy = (topTenEconomicalBaller[index2].runs * 6) / topTenEconomicalBaller[index2].ball;
                            topTenEconomicalBaller[index2].economy = curEconomy;
                            found = true;
                        }
                    }
                    //if data is not present in answer array
                    if (!found) {
                        let curEconomy = (curRun * 6) / 1;
                        let newBowler = {
                            bowler: curBowler,
                            runs: curRun,
                            ball: 1,
                            economy: curEconomy
                        }
                        topTenEconomicalBaller.push(newBowler);
                    }

                }
            }
            // sorting the answer array to get top ten economical bowler!
            const ans = topTenEconomicalBaller.sort((prev, cur) => {
                if (prev.economy > cur.economy) {
                    return 1;
                }
                else if (prev.economy < cur.economy) {
                    return -1;
                }
                else {
                    return 0;
                }
            }).slice(0, 10);

            console.log(ans);
            fs.writeFileSync('src/public/output/4-top-10-economical-bowlers-in-the-year-2015.json', JSON.stringify(ans));
        });
}

demo();