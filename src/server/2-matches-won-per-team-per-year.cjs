const csvToJson = require('csvtojson');
const matchesPath = 'src/data/matches.csv';
const fs = require('fs');
// fetching data from matches.csv file
const demo = () => {
    csvToJson()
        .fromFile(matchesPath)
        .then((data) => {

            const dataArr = [];
            for (let index = 0; index < data.length; index++) {

                let winner = data[index].winner;
                let year = data[index].season;
                let found = false;
                // if data is present in answer array!
                for (let index2 = 0; index2 < dataArr.length; index2++) {
                    if (dataArr[index2].year == year && dataArr[index2].team == winner) {
                        dataArr[index2].totalWins += 1;
                        found = true;
                        break;
                    }
                }

                // if data is not present in the answer array!
                if (!found) {
                    let newDataEntry = {
                        year: data[index].season,
                        team: data[index].winner,
                        totalWins: 1
                    };
                    dataArr.push(newDataEntry);
                }
            }

            // cross-checking the answer by printing the array.
            console.log(dataArr);

            fs.writeFileSync("src/public/output/2-matches-won-per-team-per-year.json", JSON.stringify(dataArr));
        });
}

demo();