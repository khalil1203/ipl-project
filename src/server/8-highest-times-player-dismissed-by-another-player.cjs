const deliveriesPath = 'src/data/deliveries.csv';

const fs = require('fs');
const csvtojson = require('csvtojson');
const player_Arr = [];

// fetching data from deliveries.csv file
csvtojson()
    .fromFile(deliveriesPath)
    .then((deliveries) => {

        for (let index = 0; index < deliveries.length; index++) {

            //if a player is dismmised
            if (deliveries[index].player_dismissed != '') {
                let found = false;

                for (let index2 = 0; index2 < player_Arr.length; index2++) {
                    if (deliveries[index].bowler == player_Arr[index2].bowler && deliveries[index].batsman == player_Arr[index2].batsmen) {
                        player_Arr[index2].count += 1;
                        found = true;
                    }
                }

                //if data is not present in the answer array!
                if (!found) {
                    let obj = {
                        batsmen: deliveries[index].batsman,
                        bowler: deliveries[index].bowler,
                        count: 1
                    }
                    player_Arr.push(obj);
                }
            }
        }

        //sorting the answer array according to dismissal count!
        player_Arr.sort((prev1, cur1) => {
            if (prev1.count < cur1.count) {
                return 1;
            }
            else if (prev1.count > cur1.count) {
                return -1;
            }
            else {
                return 0;
            }
        });

        console.log(player_Arr);
        fs.writeFileSync('src/public/output/8-highest-times-player-dismissed-by-another-player.json', JSON.stringify(player_Arr));
    });

