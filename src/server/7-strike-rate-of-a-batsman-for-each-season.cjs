const deliveriesPath = 'src/data/deliveries.csv';
const matchesPath = 'src/data/matches.csv';
const fs = require('fs');
const csvtojson = require('csvtojson');

const batsmen_Arr = [];
const matchId_Arr = [];

// fetching data from matches.csv file
csvtojson()
    .fromFile(matchesPath)
    .then((data) => {

        matchId_Arr.push(0);
        for (let index = 0; index < data.length; index++) {
            matchId_Arr.push(data[index].season);
        }
        console.log(matchId_Arr);
    });

// fetching data from deliveries.csv file
csvtojson()
    .fromFile(deliveriesPath)
    .then((deliveries) => {

        for (let index = 0; index < deliveries.length; index++) {
            let id = parseInt(deliveries[index].match_id);
            let year = parseInt(matchId_Arr[id]);
            let curbatsmen = deliveries[index].batsman;
            let curRuns = parseInt(deliveries[index].batsman_runs);
            let found = false;

            for (let index2 = 0; index2 < batsmen_Arr.length; index2++) {
                if (batsmen_Arr[index2].season == year && batsmen_Arr[index2].player == curbatsmen) {
                    batsmen_Arr[index2].runs += curRuns;
                    batsmen_Arr[index2].ball += 1;
                    found = true;
                    batsmen_Arr[index2].strikeRate = (batsmen_Arr[index2].runs * 100) / batsmen_Arr[index2].ball;
                    break;
                }
            }
            //if data is not present in answer array!
            if (!found) {
                batsmen_Arr.push({
                    season: year,
                    player: curbatsmen,
                    runs: curRuns,
                    ball: 1,
                    strikeRate: curRuns * 100
                });
            }
        }
        // sorting the answer array in ascending order according to the season!
        const ans = batsmen_Arr.sort((prev1, cur1) => {
            if (prev1.season > cur1.season) {
                return 1;
            }
            else if (prev1.season < cur1.season) {
                return -1;
            }
            else {
                return 0;
            }
        });

        // cross-checking the answer!
        console.log(ans);
        fs.writeFileSync('src/public/output/7-strike-rate-of-a-batsman-for-each-season.json', JSON.stringify(batsmen_Arr));
    });

