const deliveriesPath = 'src/data/deliveries.csv';
const fs = require('fs');
const csvtojson = require('csvtojson');
const bestPlayer_Arr = [];

// fetching data from deliveries.csv file
csvtojson()
    .fromFile(deliveriesPath)
    .then((deliveries) => {

        for (let index = 0; index < deliveries.length; index++) {
            //if it is an super over !!
            if (deliveries[index].is_super_over != '0') {
                let found = false;

                for (let index2 = 0; index2 < bestPlayer_Arr.length; index2++) {
                    if (deliveries[index].bowler == bestPlayer_Arr[index2].bowler) {


                        bestPlayer_Arr[index2].runs += parseInt(deliveries[index].total_runs);
                        bestPlayer_Arr[index2].ball += 1;

                        let tempEconomy = (bestPlayer_Arr[index2].runs * 6) / bestPlayer_Arr[index2].ball;
                        bestPlayer_Arr[index2].economy = tempEconomy;
                        found = true;
                    }
                }
                //if data is not present in the answer array
                if (!found) {
                    let obj = {
                        bowler: deliveries[index].bowler,
                        runs: parseInt(deliveries[index].total_runs),
                        ball: 1,
                        economy: deliveries[index].total_runs * 6
                    }
                    bestPlayer_Arr.push(obj);
                }
            }
        }
        //sorting the answer array according to the economy of bowler in super over!!
        bestPlayer_Arr.sort((prev1, cur1) => {
            if (prev1.economy > cur1.economy) {
                return 1;
            }
            else if (prev1.economy < cur1.economy) {
                return -1;
            }
            else {
                return 0;
            }
        });

        console.log(bestPlayer_Arr);
        fs.writeFileSync('src/public/output/9-bowler-with-best-economy-in-super-overs.json', JSON.stringify(bestPlayer_Arr[0]));
    });

