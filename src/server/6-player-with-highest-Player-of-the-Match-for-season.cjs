const csvToJson = require('csvtojson');
const matchesPath = 'src/data/matches.csv';
const fs = require('fs');

const demo = () => {

    // fetching data from matches.csv file
    csvToJson()
        .fromFile(matchesPath)
        .then((data) => {
            let ansData = [];

            data.map((playerData) => {

                found = false;

                for (let index = 0; index < ansData.length; index++) {
                    if (ansData[index].season == playerData.season && ansData[index].player == playerData.player_of_match) {
                        ansData[index].motm += 1;
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    let demoObj = {
                        season: playerData.season,
                        player: playerData.player_of_match,
                        motm: 1
                    }
                    ansData.push(demoObj);
                }


            });


            ansData.sort((prev, cur) => {
                if (prev.season > cur.season) {
                    return 1;
                }
                else if (prev.season < cur.season) {
                    return -1;
                }
                else {
                    return 0;
                }
            });

            let finalData = [];

            //    let curYear = ansData[index].season;
            let player = ansData[0].player;
            let count = ansData[0].motm;
            let season = ansData[0].season;

            for (let index = 1; index < ansData.length; index++) {
                let curYear = ansData[index].season;
                let curPlayer = ansData[index].player;
                let curCount = ansData[index].motm;

                if (curYear != season) {
                    finalData.push({
                        season: season,
                        player: player,
                        motm: count
                    });
                    season = curYear;
                    count = curCount;
                    player = curPlayer;
                    continue;
                }

                if (curCount > count) {
                    count = curCount;
                    player = curPlayer;
                    continue;
                }

            }
            finalData.push({
                season: season,
                player: player,
                motm: count
            });

            console.log(finalData);
            fs.writeFileSync('src/public/output/6-player-with-highest-Player-of-the-Match-for-season.json', JSON.stringify(finalData));
        });
}
demo();

