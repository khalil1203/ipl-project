const csvToJson = require('csvtojson');
const matchesPath = 'src/data/matches.csv';
const fs = require('fs');

//fetching data from matches.csv file
const demo = () => {
    csvToJson()
        .fromFile(matchesPath)
        .then((data) => {
            let matchesArr = [];
            for (let index = 0; index < data.length; index++) {
                matchesArr.push(data[index]);
            }

            const matchPerYear = {};
            matchesArr.map((data) => {
                if (matchPerYear[data.season] == undefined) {
                    matchPerYear[data.season] = 1;
                }
                else {
                    matchPerYear[data.season] = (matchPerYear[data.season]) + 1;
                }
            });

            // getting output to new .json file
            fs.writeFileSync('src/public/output/1-matches-per-year.json', JSON.stringify(matchPerYear));
        });
}

demo();