const csvToJson = require('csvtojson');
const deliveriesPath = 'src/data/deliveries.csv';
const matchesPath = 'src/data/matches.csv';
const fs = require('fs');
const match_Id_Arr = [];


const demo = () => {

    //fetching data from matches.csv file
    csvToJson()
        .fromFile(matchesPath)
        .then((data) => {
            for (let index = 0; index < data.length; index++) {
                if (data[index].season == '2016')
                    match_Id_Arr.push(data[index].id);
            }
            console.log(match_Id_Arr);
        });
    // fetching data from deliveries.csv file
    csvToJson()
        .fromFile(deliveriesPath)
        .then((deliveriesData) => {
            let ExtraRunsPerTeam = [];

            for (let index = 0; index < deliveriesData.length; index++) {

                //cross-checking the id from matches.csv data!!
                if (match_Id_Arr.includes(deliveriesData[index].match_id)) {
                    let found = false;
                    for (let index2 = 0; index2 < ExtraRunsPerTeam.length; index2++) {

                        // if data is found in answer array!
                        if (ExtraRunsPerTeam[index2].team == deliveriesData[index].bowling_team) {
                            ExtraRunsPerTeam[index2].runs = parseInt(ExtraRunsPerTeam[index2].runs) + parseInt(deliveriesData[index].extra_runs);
                            found = true;
                        }
                    }

                    // if data is not found then create new object!
                    if (!found) {
                        const obj = {
                            season: 2016,
                            team: deliveriesData[index].bowling_team,
                            runs: deliveriesData[index].extra_runs
                        }
                        ExtraRunsPerTeam.push(obj);
                    }
                }
            }

            console.log(ExtraRunsPerTeam);
            fs.writeFileSync('src/public/output/3-extra-runs-per-team-2016.json', JSON.stringify(ExtraRunsPerTeam));
        });
}

demo();